'''
Created on 2019. 10. 30.

@author: tobew
conda create -n bert_ve37 python=3.7
pip install --pre --upgrade mxnet-cu100
pip install -U https://github.com/dmlc/gluon-nlp/archive/master.zip

https://gist.github.com/haven-jeon/3d7c538398e93dab2ed4899159a5d943
Vocab file is not found. Downloading.
https://github.com/apache/incubator-mxnet/issues/4431
https://gluon-nlp.mxnet.io/
'''
import pandas as pd
import numpy as np
from mxnet.gluon import nn, rnn
from mxnet import gluon, autograd
import gluonnlp as nlp
from mxnet import nd 
import mxnet as mx
import time
import itertools
import random
from os import listdir
from os.path import isfile, join

class BERTDataset(mx.gluon.data.Dataset):
    # BERT embedding을 이용한 데이터셋 만들기 
    def __init__(self, dataset, sent_idx, label_idx, bert_tokenizer, max_len,
                 pad, pair):
        transform = nlp.data.BERTSentenceTransform(
            bert_tokenizer, max_seq_length=max_len, pad=pad, pair=pair)
        sent_dataset = gluon.data.SimpleDataset([[
            i[sent_idx],
        ] for i in dataset])
        self.sentences = sent_dataset.transform(transform)
        self.labels = gluon.data.SimpleDataset(
            [np.array(np.int32(i[label_idx])) for i in dataset])

    def __getitem__(self, i):
        return (self.sentences[i] + (self.labels[i], ))

    def __len__(self):
        return (len(self.labels))
    

class BERTClassifier(nn.Block):
    # BERT embedding을 이용한 분류기 클래스 
    def __init__(self,
                 bert,
                 num_classes=256,
                 dropout=None,
                 prefix=None,
                 params=None):
        super(BERTClassifier, self).__init__(prefix=prefix, params=params)
        self.bert = bert
        with self.name_scope():
            self.classifier = nn.HybridSequential(prefix=prefix)
            if dropout:
                self.classifier.add(nn.Dropout(rate=dropout))
            self.classifier.add(nn.Dense(units=num_classes))

    def forward(self, inputs, token_types, valid_length=None):
        _, pooler = self.bert(inputs, token_types, valid_length)
        return self.classifier(pooler)
    

def load_model():
    global ctx
    global bert_base
    global vocabulary

        # gpu 사용하도록 설정 
        # pip install --pre --upgrade mxnet-cu100
        # 위의 option에서 cuda library 버전을 맞춰야 함...  cuda library 버전10.0 인 경우,  9.0 이면  cu90 이런 식으로 바꿔야 함 
    ctx = mx.gpu() 
    #mxnet.base.MXNetError: [07:43:15] C:\Jenkins\workspace\mxnet\mxnet\src\ndarray\ndarray.cc:1295: GPU is not enabled
    bert_base, vocabulary = nlp.model.get_model('bert_12_768_12',
                                                 dataset_name='wiki_multilingual_cased',
                                                 pretrained=True, ctx=ctx, use_pooler=True,
                                                 use_decoder=False, use_classifier=False)
    # bert 구조 확인용
    print(bert_base)
    print('---------') 



def main():
    files = [f for f in listdir('params') if isfile(join('params', f))]
    for i in range(len(files)):
        
        er =np.loadtxt("data/256_label_info.txt" ,encoding='utf-8' ,delimiter='\t',skiprows=1,dtype=str)
        
        
        
        
        #=======================================================================
        # # gpu 사용하도록 설정 
        # # pip install --pre --upgrade mxnet-cu100
        # # 위의 option에서 cuda library 버전을 맞춰야 함...  cuda library 버전10.0 인 경우,  9.0 이면  cu90 이런 식으로 바꿔야 함 
        # ctx = mx.gpu() 
        # #mxnet.base.MXNetError: [07:43:15] C:\Jenkins\workspace\mxnet\mxnet\src\ndarray\ndarray.cc:1295: GPU is not enabled
        # bert_base, vocabulary = nlp.model.get_model('bert_12_768_12',
        #                                              dataset_name='wiki_multilingual_cased',
        #                                              pretrained=True, ctx=ctx, use_pooler=True,
        #                                              use_decoder=False, use_classifier=False)
        # # bert 구조 확인용
        # print(bert_base)
        # print('---------')
        #=======================================================================
        # 단순한 데이터를 bert를 이용하여 변환 및 출력한 예, 확인용
    #     ds = gluon.data.SimpleDataset([['나 보기가 역겨워', '김소월']])    
    #     tok = nlp.data.BERTTokenizer(vocab=vocabulary, lower=False)    
    #     trans = nlp.data.BERTSentenceTransform(tok, max_seq_length=10)
    #     
    #     print(list(ds.transform(trans)))  # bert를 이용하여 변환...   
    #         
            
        #https://github.com/e9t/nsmc  
        #https://github.com/e9t/nsmc  
        # tsv 파일에서  0,1,2 컬럼에서  1이 문장, 2sms label임
        # 데이터셋 생성  
        #dataset_train = nlp.data.TSVDataset("ratings_train.txt", field_indices=[1,2], num_discard_samples=1)
        #dataset_test = nlp.data.TSVDataset("ratings_test.txt", field_indices=[1,2], num_discard_samples=1)
     
     
        #  법률 데이터셋 생성  
        #dataset_train = nlp.data.TSVDataset("oneinall/30to40_train.txt", field_indices=[0,1])
        dataset_test = nlp.data.TSVDataset("data/256_test.txt", field_indices=[0,1])
        
            
        # bert tokenizer 생성 
        bert_tokenizer = nlp.data.BERTTokenizer(vocabulary, lower=False)
        # 길이에 따라 사용하는 메모리의 크기가 달라짐 
        # 메모리가 부족한 경우 max_len 길이를 늘여야 함.. 주의!!! 
    #     max_len = 64
        max_len = 128
    
        data_test = BERTDataset(dataset_test, 0, 1, bert_tokenizer, max_len, True, False)
        model = BERTClassifier(bert_base, num_classes=256, dropout=0.3)
        batch_size = 1
        test_dataloader = mx.gluon.data.DataLoader(data_test, batch_size=batch_size, num_workers=5)

        model.load_parameters("params/"+files[i] ,ctx = ctx)
        params_name = files[i]
        print(params_name)
        #data_iter = mx.gluon.data.DataLoader(sentences, batch_size=10, num_workers=1)
        # 정확도 평가를 위한 내부 함수 
        def infer_all(model, data_iter, ctx=ctx):
            #acc = mx.metric.Accuracy()
            i = 0
            k=0
            wrong =[]
            right=[]
            for i, (t,v,s, label) in enumerate(data_iter):
                print(k)
                token_ids = t.as_in_context(ctx)
                valid_length = v.as_in_context(ctx)
                segment_ids = s.as_in_context(ctx)
                label = label.as_in_context(ctx)
                output = model(token_ids, segment_ids, valid_length.astype('float32'))
                #acc.update(preds=output, labels=label)
                #print('#################3output###############')
                #print(output)
    
                
                dic = {} 
                data = dataset_test[i]
    
                y = nd.softmax(output[0])            
                y = y.asnumpy()
                
                result= []    
                for w in range(len(y)) :
                    result.append(y[w])             
                
                for i in range(len(result)):
                    dic[result[i]] = er[i][1]            
    
                rank = sorted(dic.items() ,reverse = True)
                #right_infer = open("infer_result/560_right_infer.txt", 'a+t',encoding='utf-8')            
                #wrong_infer = open("infer_result/560_wrong_infer.txt", 'a+t',encoding='utf-8')
                #all_infer= open("infer_result/560_allinfer.txt", 'a+t',encoding='utf-8')   
                test_percent= open("data/addtest_param_refer_add.txt", 'a+t',encoding='utf-8')           
                print("recoding this")            
                label_n = int(data[1])
                print(rank[0][0], "\t",rank[0][1],"\t",er[label_n][1],"\t",data[0],"\t",data[1])
                
                sim  = str(rank[0][0]) #유사도
                infer =  str(rank[0][1])# 추론결과
                r_an = str(er[label_n][1]) #정답계약서
                r_label = str(data[1]) # 질의문
                print("###########label#################")
                print(label ,r_label) 
                

                #print(valid_length[0] ,"length")
                if rank[0][1] != er[label_n][1]:
                    wrong.append(er[label_n][1])
                    #wrong_infer.write("[*]"+sim+ "\t"+infer+"\t"+r_an+"\t"+data[0]+"\t"+r_label+"\n") 
                    #all_infer.write("[*]"+sim+ "\t"+infer+"\t"+r_an+"\t"+data[0]+"\t"+r_label+"\n")
                    #wrong_infer.close()    
                    #all_infer.close()
                if rank[0][1] == er[label_n][1]:
                    right.append(er[label_n][1])
                    #right_infer.write(sim+ "\t"+infer+"\t"+r_an+"\t"+data[0]+"\t"+r_label+"\n") 
                    #all_infer.write(sim+ "\t"+infer+"\t"+r_an+"\t"+data[0]+"\t"+r_label+"\n") 
                    #all_infer.close()
                    #right_infer.close()  
                       #f1.update(preds=output, labels=label)
                #===================================================================
                # if i > 1000:
                #     break
                # i += 1
                #===================================================================
                k += 1 
            perc = len(right) /(len(wrong)+len(right))
            print(len(right),len(wrong))
                
            print(perc)
            print("Test accuracy : " ,str(perc) ,"\t",params_name)
            test_percent.write("Test accuracy : " + str(perc)+params_name + "\n") 
    
        
    
        test_acc = infer_all(model, test_dataloader, ctx)
        
        #txt.write('Test Acc ' +" : "+str(test_acc)+"   "+'\n')
            # epoch 마다 테스트 정확도를 출력 
        #print('Test Acc : {}'.format(test_acc))
        #model.save_parameters('diff_amount/')
        #print(metric)    
    
        
    
if __name__=='__main__':
    load_model()
    main()
    
    
    
