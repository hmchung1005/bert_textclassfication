from __future__ import print_function
import json
import mxnet as mx
from mxnet import nd, gluon
import numpy as np
from mxnet.gluon import nn
import gluonnlp as nlp
from konlpy.tag import Kkma
from konlpy.utils import pprint
import timeit
from hunspell import Hunspell



start = timeit.default_timer()

def load_model():
    global new_model
    global vocabulary
    global ctx
    global er
    
    er =np.loadtxt("data/256_label_info.txt" ,encoding='utf-8' ,delimiter='\t',skiprows=1,dtype=str)
    ctx = mx.gpu()
    bert_base, vocabulary = nlp.model.get_model('bert_12_768_12',
                                                 dataset_name='wiki_multilingual_cased',
                                                 pretrained=True, ctx=ctx, use_pooler=True,
                                                 use_decoder=False, use_classifier=False)  
    
    
    new_model = BERTClassifier(bert_base, num_classes=256, dropout=0.3) 

    new_model.load_parameters("params/final_70" ,ctx = ctx)
    print("load_model")
    
    
class BERTClassifier(nn.Block):
    # BERT embedding을 이용한 분류기 클래스 
    def __init__(self,
                 bert,
                 num_classes=256,
                 dropout=None,
                 prefix=None,
                 params=None):
        super(BERTClassifier, self).__init__(prefix=prefix, params=params)
        self.bert = bert
        with self.name_scope():
            self.classifier = nn.HybridSequential(prefix=prefix)
            if dropout:
                self.classifier.add(nn.Dropout(rate=dropout))
            self.classifier.add(nn.Dense(units=num_classes))

    def forward(self, inputs, token_types, valid_length=None):
        _, pooler = self.bert(inputs, token_types, valid_length)
        return self.classifier(pooler)


def main(test_text):

    kkma = Kkma() # 명사 분석기
    spell_check=Hunspell('ko',hunspell_data_dir='ko_dic') # 오타 점검
    
    
    def inference(model, data_iter, ctx=ctx):
        i = 0
        for i, (t,v,s) in enumerate(data_iter):
             
            token_ids = t.as_in_context(ctx)   #inputs
            valid_length = v.as_in_context(ctx) # valid_length
            segment_ids = s.as_in_context(ctx) # token_types
            output = model(token_ids, segment_ids, valid_length.astype('float32'))
            #model.summary(token_ids,segment_ids)
            if i > 1000:
                break
            i += 1
            
        y = nd.softmax(output[0])            
        return y  
    
 

    
    #########verify new_model##################################
    b = nlp.data.BERTTokenizer(vocabulary, lower=False)
    
    transform = nlp.data.BERTSentenceTransform(
            b, max_seq_length=128, pad=True, pair=False)
    sent_dataset = gluon.data.SimpleDataset([[test_text]])
       
    sentences = sent_dataset.transform(transform)        

    data_iter = mx.gluon.data.DataLoader(sentences, batch_size=50, num_workers=1)    
    

                 
    dic = {}    
    infer = inference(new_model,data_iter, ctx)
    infer = infer.asnumpy()
    result= []    
    for w in range(len(infer)) :
        result.append(infer[w])
    
    print("###")
    print(test_text)
    
    for i in range(len(result)):
        dic[result[i]] = er[i]
        
    #print("####dic###")
    #print(dic)
    rank = sorted(dic.items() ,reverse = True)
    print("#####model_infer##########")  
    recom_contract = []  #0.1퍼센트 이상의 유사도를 가진 계약서들
    
    set_contract = [[33,22], [44, 45], [11,0],[118,120],[202,78],[100,105]]



# 1순위 추론 계약서 레이블 넘버
    infer_result = int(rank[0][1][0]) 
    print(infer_result)        
###### 추천계약서에 append 시작#######     
    
    for ra in range(0,3):
        #if round(rank[ra][0]*100,2) >= 0.1:
            print(str(ra+1)+"위   : " , str(round(rank[ra][0]*100,2))+"%",rank[ra][1][1])
            recom_contract.append(rank[ra][1][1])

#### set인 아이들은 동시에 추천해주기
###### 현재 세개만 보여줄 것이기떄문에 따로 처리 안하지만 세 개 이상 보여주는 거면 조건 하나 더 추가해야함  
    for a in range(len(set_contract)) :
        if infer_result in set_contract[a] : #and len([x for x in recom_contract if x in set_contract[a]]) == 0 :
            set_another = [s for s in set_contract[a] if infer_result != s]
            recom_contract.insert(1,er[set_another[0]][1])
            
    
            
###########################################   
    group = json.load(open('json/group_dic.json', 'r', encoding='utf-8'))
    
    noun_list=[]
    pos = kkma.pos(test_text) # 질의문의 명사 추출

    for keyword , type in pos:
        if type == "NNG" or type == "NNP":
            noun_list.append(keyword)
########### 키워드 검사 ############################


#################################################
    # 1순위로 추론된 계약서의 상위 클래스의 json 파일 가져오기
    
    for key, val in group.items():
        
        if infer_result in val :
            json_file = key
            seg_dic = json.load(open('json/' + str(json_file)+'.json', 'r', encoding='utf-8'))
            
            upper_class = key
            print("upper_class"  , upper_class)
            uppper_contract = er[int(upper_class)][1]
            print("upper_contract"  ,uppper_contract)
            if uppper_contract not in recom_contract[0:3]:
                recom_contract.insert(1,er[int(upper_class)][1])
               
        else : # 
            json_file = None               
            #print("nothing")
    
           
    print("###### final rocommendation ######")
    for i in range(0,3) :
        print(recom_contract[i])            
            
        #=======================================================================
        #     keys =[] 
        #     
        #     for key, val in seg_dic.items():
        #         for noun in noun_list :
        #             if noun in val :
        #                 print(key)
        #                 keys.append(key)
        # 
        #     if len(keys) == 0: # 어느 곳에도 속하지 않았을 시 
        #         print("## 상위 클래스##")
        #         
        #=======================================================================
            
                #print(list(seg_dic.keys())[0]) # 첫번째 레이블이 가장 상위 계약서
                #json_file = 
                #print("nothing")



####################################################################################

    #===========================================================================
    # else: 
    #     print("  ")
    #     print("==========================")
    #     print("추론 결과 : " , rank[0][1])
    # 
    #===========================================================================
    
    stop = timeit.default_timer()
    print(stop - start)
    
if __name__=='__main__':
    load_model()
    txt = None
    while txt != "x":
        print("질의문을 입력하세요")
        txt = input(">>")
        print("입력완료")
        print("추론 중입니다.....")
        main(txt) 
        print(" ")   
        print("추론 완료")
        