from hunspell import Hunspell
from konlpy.tag import Kkma , Okt
from konlpy.utils import pprint
import string
import re


kkma = Kkma()



#j = Hunspell('ko', hunspell_data_dir='C:/Users/T3QRND03/Downloads/new')

# 일반 hunspell은 영어
# ko_dic 파일안에는 ko.aff 와 ko.dic 파일 존재
# ko.aff 와 ko.dic은 깃허브에서 다운 가능
j = Hunspell('ko', hunspell_data_dir='ko_dic/')



#sent = "안녕 하세요. 저는 한국인 입니다. 이문장는 한글로 작성됬습니다."
#sent = "이제까지 나에게 없었떤 일이었다."
#sent = "안녕 하세요. 저는 한국인 입니다. 이 문장는 한글로 작성됬습니다. 저는 집 을 사고 싶어요."
#sent = "집 을 사고 싶어요"
#sent = "몇 시  몇 분 "
#sent = "이 커피는 맛이 없어"
#sent = "아파트를 메매로 사려구요."
#sent = "이커피는 맛이 없어"
#sent = '(이하 "갑"이라 칭한다) joint venture계약서 123원을 &(&%6 6468 만원 '


# 숫자를 한글로 바꾸어 주는 함수 -> 최종에 적용은 안함
def digit2txt(strNum):
    tenThousandPos = 4
    # 억 단위 자릿수
    hundredMillionPos = 9
    txtDigit = ['', '십', '백', '천', '만', '억']
    txtNumber = ['', '일', '이', '삼', '사', '오', '육', '칠', '팔', '구']
    txtPoint = '쩜 '
    
    resultStr = ''
    digitCount = 0
    print(strNum)
    #자릿수 카운트
    for ch in strNum:
        # ',' 무시
        if ch == ',':
            continue
        #소숫점 까지
        elif ch == '.':
            break
        digitCount = digitCount + 1
 
 
    digitCount = digitCount-1
    index = 0
 
    while True:
        notShowDigit = False
        ch = strNum[index]
        #print(str(index) + ' ' + ch + ' ' +str(digitCount))
        # ',' 무시
        if ch == ',':
            index = index + 1
            if index >= len(strNum):
                break;
            continue
 
        if ch == '.':
            resultStr = resultStr + txtPoint
        else:
            #자릿수가 2자리이고 1이면 '일'은 표시 안함.
            # 단 '만' '억'에서는 표시 함
            if(digitCount > 1) and (digitCount != tenThousandPos) and  (digitCount != hundredMillionPos) and int(ch) == 1:
                resultStr = resultStr + ''
            elif int(ch) == 0:
                resultStr = resultStr + ''
                # 단 '만' '억'에서는 표시 함
                if (digitCount != tenThousandPos) and  (digitCount != hundredMillionPos):
                    notShowDigit = True
            else:
                resultStr = resultStr + txtNumber[int(ch)]
 
 
        # 1억 이상
        if digitCount > hundredMillionPos:
            if not notShowDigit:
                resultStr = resultStr + txtDigit[digitCount-hundredMillionPos]
        # 1만 이상
        elif digitCount > tenThousandPos:
            if not notShowDigit:
                resultStr = resultStr + txtDigit[digitCount-tenThousandPos]
        else:
            if not notShowDigit:
                resultStr = resultStr + txtDigit[digitCount]
 
        if digitCount <= 0:
            digitCount = 0
        else:
            digitCount = digitCount - 1
        index = index + 1
        if index >= len(strNum):
            break;
    #print(resultStr)
    return resultStr

def main(sent):

    numbers = re.findall("\d+", sent) # 문장에서 숫자만 추출
    print("numbers" , numbers) 
    nouns = []  # 문장에서 명사만 추출
    pos = kkma.pos(sent)
    
    for keyword, type in pos:
        if type == "NNG":
            nouns.append(keyword)
    
    print("nouns" , nouns) 
    split = sent.split(" ") # 문장 띄어쓰기 단위로 나눔
    #print(z)
    split = [i for i in split if i !=""] #  두번 띄어쓴 부분 삭제
    print(split)
    
    #text = re.sub('[-=+,#/\?:^$.@*\"※~&%ㆍ!』\\‘|\(\)\[\]\<\>`\'…》]', '', sent)
    hangul = re.compile('[^ ㄱ-ㅣ가-힣]+') # 한글을 제외한 한자,영어,기호 발라내기
    engAndsign = hangul.findall(sent) # 한글과 붙어있는 영어 또는 한자를 hunspell에서 삭제하지 못하게 하기 위해
    
    print("engAndsign" , engAndsign)
    
    #for i in range(len(engAndsign)):
    set = [x for x in nouns if x in split]
    
    print("set")
    print(set)
    print("####")
    #####################################################################
    # hunspell은 숫자와 문자가 붙어 있으면 숫자를 삭제해 버림
    # 숫자 한글로 변경
    #===============================================================================
    # for i in numbers:
    #     try:
    #         qq = digit2txt(i)
    #         print("qq" ,qq)
    #         for k in range(len(split)):                       # 10과 100일 겹칠 수 도 있기 때문에 숫자를 한글로 바꾸어주고 나면 0,1,2,3,4,5,6,7,8,9 가 없어야 한다.
    #             if i in split[k] and ("." or "/" or "-") not in split[k] and len(re.findall("\d+", split[k].replace(i,digit2txt(i))))==0 : #"." not in split[k]는 날짜인 경우는 한글로 바꾸어 주지 않는다.
    #                 print(split[k])
    #                 split[k] = split[k].replace(i,digit2txt(i)+" ")
    #                 print(split[k])
    #     except:
    #         continue
    #===============================================================================
    
    
    final = []
    del_nu = [] 
    print("final_split" , split)
    
    # ex>["안녕", "하세요."] ->["안녕하세요."] 로 바꿔주기
    # 명사 추출한 리스트(nouns)와 띄어쓰기로 만든 리스트(z)의 공통 요소가 있는 리스트 생성(set)
    # set 요소를 가진 z의 인덱스("안녕")과 그 인덱스+1("하세요") 붙여준다 -> 만약 틀린 붙임이라면 hunspell이 띄어쓰기 처리 해줌.
    # 예외 상황 ex> "집 을 사고 싶어요." 일 때 nouns = ["집" , "을"]
    # 예외 상황일 경우 
    for i in range(len(split)):
    # 띄어쓰기 리스트가 set에 있으면 그 다음 인덱스와 더해준다. 더해준 인덱스는 
    # del_nu에 더해서 최종 단어 구성에 중복되어 들어가지 않게 한다. 
    #명사 추출에 존재하는 단어일 때 붙여준다. 명사+'한다','까지' 등을 잘 못 띄어쓰기 하였을 때 붙이기   
        if split[i] in set and i not in del_nu  : 
            nu = split.index(split[i])
            print(split[i], i ,nu ,len(split))
            nnu = nouns.index(split[i])
            if nu != len(split)-1  : 
                final.append(split[i]+split[nu+1])
                del_nu.append(nu+1)
            else :
                final.append(split[i]) 
            
        elif split[i] not in set and i not in del_nu:
            final.append(split[i])
    
    print("final1")
    print(final)
    
    
    # 계약시 같은 경우 hunspell 에서 '계약 시'의 단어를 가장 나중에 추천 해줌. 그렇기 때문에 명사로 떨어져 나왔는데 붙어 있는것은
    # 붙여 주어야 한다. ex> 계약시 입력시 konlpy 는 "계약" "시" 라고 해주는 데 이게 맞는말임
    # 명사는 모두 띄어 쓰기 해야하는것이 규칙
    for i in range(len(nouns)-1):
        if nouns[i]+nouns[i+1] in final and nouns[i+1] == "시":
            let_sp = nouns[i]+nouns[i+1]
            del_in = final.index(let_sp) # "계약시" 인덱스 찾기 => 쪼개 놔야 하니까
            final[del_in] = nouns[i] # "계약시" 자리에 "계약" 넣기
            final.insert(final.index(nouns[i])+1,nouns[i+1]) # "계약" 다음 인덱스 "시" insert
            
    print("final2")
    print(final)
    
    # 고쳐진 문장 두 개까지 보여줌
    # hunspell은 마침표 찍어주지 않음
    # 숫자,영어,기호 ,한자,특수문자가 붙어있는 문자는 고쳐주지 않음.
    adj = []
    adj2 = []
    for i in range(len(final)):
        q = j.suggest(final[i])
        print(final[i] ,"|",q)
        notHangul = hangul.findall(final[i])
        print(len(notHangul)) # 점을 제외한 특수문자,한자,영어가 있으면 자기  고치지 않고 자신을 쓴다.
        matching = [s for s in notHangul if "." not in s] 
        print(matching)
        # 날짜 같은 경우는 2019.03.10 은 자기 자신 그대로 사용  
        if len(q) != 0 and len(matching) ==0  : 
        # 띄어쓰기를 제외한 오탈자가 고쳐졌을 시에 해당 단어에 "*" 붙여주기
        # . 이 찍혀져 있는 것은 그대로 기록하기
        # 지문 특성상 . 외의 특수문자는 오기 힘듦..(있나..?)
            if final[i].strip(string.punctuation).replace(" ","") != q[0].strip(string.punctuation).replace(" ","") :
                if final[i] != final[i].strip(string.punctuation):
                    adj.append("*"+q[0]+".")
                    if len(q) >= 2 :
                        adj2.append("*"+q[1]+".")
                    else :
                        adj2.append("*"+q[0]+".")
                else:
                    adj.append("*"+q[0])
                    if len(q) >= 2 :
                        adj2.append("*"+q[1])
                    else : 
                        adj2.append("*"+q[0])
            else :
                if final[i] != final[i].strip(string.punctuation) :
                    adj.append(final[i])
                    adj2.append(final[i])
                else:
                    adj.append(q[0])
                    adj2.append(q[0])
        else:
            adj.append(final[i])
            adj2.append(final[i])
        
    sente = ""
    sente2 = ""
    for i in range(len(adj)):
        sente = sente +" "+adj[i]
        sente2 = sente2 +" "+adj2[i]
    
    print(adj ,adj2)
    print("###input sentence#########")
    print(sent)
    print("                           ")
    print("###correct sentence########")
    if sente.strip() == sente2.strip() :
        print(sente.strip())
    else:
        print(sente.strip())
        print(sente2.strip())
    
             
if __name__=='__main__':
    txt = input(">>")
    print("입력완료")
    print("교정 중입니다.....")
    main(txt) 
    print(" ")   
    print("교정 완료")      
 

 
 
 
