'''
Created on 2019. 10. 30.

@author: tobew
conda create -n bert_ve37 python=3.7
pip install --pre --upgrade mxnet-cu100
pip install -U https://github.com/dmlc/gluon-nlp/archive/master.zip

https://gist.github.com/haven-jeon/3d7c538398e93dab2ed4899159a5d943
Vocab file is not found. Downloading.
https://github.com/apache/incubator-mxnet/issues/4431
https://gluon-nlp.mxnet.io/
'''
import pandas as pd
import numpy as np
from mxnet.gluon import nn, rnn
from mxnet import gluon, autograd
import gluonnlp as nlp
from mxnet import nd 
import mxnet as mx
import time
import itertools
import random


class BERTDataset(mx.gluon.data.Dataset):
    # BERT embedding을 이용한 데이터셋 만들기 
    def __init__(self, dataset, sent_idx, label_idx, bert_tokenizer, max_len,
                 pad, pair):
        transform = nlp.data.BERTSentenceTransform(
            bert_tokenizer, max_seq_length=max_len, pad=pad, pair=pair)
        sent_dataset = gluon.data.SimpleDataset([[
            i[sent_idx],
        ] for i in dataset])
        self.sentences = sent_dataset.transform(transform)
        self.labels = gluon.data.SimpleDataset(
            [np.array(np.int32(i[label_idx])) for i in dataset])

    def __getitem__(self, i):
        return (self.sentences[i] + (self.labels[i], ))

    def __len__(self):
        return (len(self.labels))
    

class BERTClassifier(nn.Block):
    # BERT embedding을 이용한 분류기 클래스 
    def __init__(self,
                 bert,
                 num_classes=2,
                 dropout=None,
                 prefix=None,
                 params=None):
        super(BERTClassifier, self).__init__(prefix=prefix, params=params)
        self.bert = bert
        with self.name_scope():
            self.classifier = nn.HybridSequential(prefix=prefix)
            if dropout:
                self.classifier.add(nn.Dropout(rate=dropout))
            self.classifier.add(nn.Dense(units=num_classes))

    def forward(self, inputs, token_types, valid_length=None):
        _, pooler = self.bert(inputs, token_types, valid_length)
        return self.classifier(pooler)
    
def main():
    # gpu 사용하도록 설정 
    # pip install --pre --upgrade mxnet-cu100
    # 위의 option에서 cuda library 버전을 맞춰야 함...  cuda library 버전10.0 인 경우,  9.0 이면  cu90 이런 식으로 바꿔야 함 
    ctx = mx.gpu() 
    #mxnet.base.MXNetError: [07:43:15] C:\Jenkins\workspace\mxnet\mxnet\src\ndarray\ndarray.cc:1295: GPU is not enabled
    bert_base, vocabulary = nlp.model.get_model('bert_12_768_12',
                                                 dataset_name='wiki_multilingual_cased',
                                                 pretrained=True, ctx=ctx, use_pooler=True,
                                                 use_decoder=False, use_classifier=False)
    # bert 구조 확인용
    print(bert_base)
    print('---------')
    # 단순한 데이터를 bert를 이용하여 변환 및 출력한 예, 확인용
#     ds = gluon.data.SimpleDataset([['나 보기가 역겨워', '김소월']])    
#     tok = nlp.data.BERTTokenizer(vocab=vocabulary, lower=False)    
#     trans = nlp.data.BERTSentenceTransform(tok, max_seq_length=10)
#     
#     print(list(ds.transform(trans)))  # bert를 이용하여 변환...   
#         
        
    #https://github.com/e9t/nsmc  
    #https://github.com/e9t/nsmc  
    # tsv 파일에서  0,1,2 컬럼에서  1이 문장, 2sms label임
    # 데이터셋 생성  
    dataset_train = nlp.data.TSVDataset("ratings_train.txt", field_indices=[1,2], num_discard_samples=1)
    dataset_test = nlp.data.TSVDataset("ratings_test.txt", field_indices=[1,2], num_discard_samples=1)
    
    # bert tokenizer 생성 
    bert_tokenizer = nlp.data.BERTTokenizer(vocabulary, lower=False)
    # 길이에 따라 사용하는 메모리의 크기가 달라짐 
    # 메모리가 부족한 경우 max_len 길이를 늘여야 함.. 주의!!! 
#     max_len = 64
    max_len = 32
    data_train = BERTDataset(dataset_train, 0, 1, bert_tokenizer, max_len, True, False)
    data_test = BERTDataset(dataset_test, 0, 1, bert_tokenizer, max_len, True, False)
    model = BERTClassifier(bert_base, num_classes=2, dropout=0.3)
    # 분류기 초기화, gpu 지정 
    model.classifier.initialize(ctx=ctx)
    model.hybridize()
    
    # softmax cross entropy loss for classification
    loss_function = gluon.loss.SoftmaxCELoss()
    # 척도를 정의    
    metric = mx.metric.Accuracy()
    
    # 메모리가 부족한 경우 batch_size를 줄여야 한다. 
#     batch_size = 64
    batch_size = 32
    lr = 5e-5
    # 데이터셋으로 부터 데이터를 load, 병렬처리를 위해서 num_worker를 5로 지정 
    train_dataloader = mx.gluon.data.DataLoader(data_train, batch_size=batch_size, num_workers=5)
    test_dataloader = mx.gluon.data.DataLoader(data_test, batch_size=batch_size, num_workers=5)
    # 학습을 위한 hyper parameter 설정 
    trainer = gluon.Trainer(model.collect_params(), 'bertadam',
                            {'learning_rate': lr, 'epsilon': 1e-9, 'wd':0.01})
    
    log_interval = 4
    num_epochs = 4
    # LayerNorm Bias Weight Decay
    for _, v in model.collect_params('.*beta|.*gamma|.*bias').items():
        v.wd_mult = 0.0
    params = [
        p for p in model.collect_params().values() if p.grad_req != 'null'
    ]
    
    # 정확도 평가를 위한 내부 함수 
    def evaluate_accuracy(model, data_iter, ctx=ctx):
        acc = mx.metric.Accuracy()
        i = 0
        for i, (t,v,s, label) in enumerate(data_iter):
            token_ids = t.as_in_context(ctx)
            valid_length = v.as_in_context(ctx)
            segment_ids = s.as_in_context(ctx)
            label = label.as_in_context(ctx)
            output = model(token_ids, segment_ids, valid_length.astype('float32'))
            acc.update(preds=output, labels=label)
            if i > 1000:
                break
            i += 1
        return(acc.get()[1])
    
    # 학습을 위한 패러미터 설정 부분
    #learning rate warmup 
    step_size = batch_size 
    num_train_examples = len(data_train)
    num_train_steps = int(num_train_examples / step_size * num_epochs)
    warmup_ratio = 0.1
    num_warmup_steps = int(num_train_steps * warmup_ratio)
    step_num = 0
    
    # 학습 수행 부분 
    for epoch_id in range(num_epochs):
        metric.reset()
        step_loss = 0
        for batch_id, (token_ids, valid_length, segment_ids, label) in enumerate(train_dataloader):
            step_num += 1
            if step_num < num_warmup_steps:
                new_lr = lr * step_num / num_warmup_steps
            else:
                offset = (step_num - num_warmup_steps) * lr / (
                    num_train_steps - num_warmup_steps)
                new_lr = lr - offset
            trainer.set_learning_rate(new_lr)
            with mx.autograd.record():
                # load data to GPU
                token_ids = token_ids.as_in_context(ctx)
                valid_length = valid_length.as_in_context(ctx)
                segment_ids = segment_ids.as_in_context(ctx)
                label = label.as_in_context(ctx)
    
                # forward computation
                out = model(token_ids, segment_ids, valid_length.astype('float32'))
                ls = loss_function(out, label).mean()
    
            # backward computation
            ls.backward()
            trainer.allreduce_grads()
            nlp.utils.clip_grad_global_norm(params, 1)
            trainer.update(token_ids.shape[0])
    
            step_loss += ls.asscalar()
            metric.update([label], [out])
            if (batch_id + 1) % (50) == 0:
                print('[Epoch {} Batch {}/{}] loss={:.4f}, lr={:.10f}, acc={:.3f}'
                             .format(epoch_id + 1, batch_id + 1, len(train_dataloader),
                                     step_loss / log_interval,
                                     trainer.learning_rate, metric.get()[1]))
                step_loss = 0
        test_acc = evaluate_accuracy(model, test_dataloader, ctx)
        # epoch 마다 테스트 정확도를 출력 
        print('Test Acc : {}'.format(test_acc))
        
        
if __name__=='__main__':
    main()      
    
    
    
