
from __future__ import print_function

import mxnet as mx
from mxnet import nd,  gluon
#from mxnet.gluon.data.vision import transforms
import numpy as np

from mxnet.gluon import nn
import gluonnlp as nlp
#import itertools
import pandas as pd
from  pandas  import DataFrame
from joblib.externals.loky.backend.context import ctx_loky


class BERTClassifier(nn.Block):
    # BERT embedding을 이용한 분류기 클래스 
    def __init__(self,
                 bert,
                 num_classes=256,
                 dropout=None,
                 prefix=None,
                 params=None):
        super(BERTClassifier, self).__init__(prefix=prefix, params=params)
        self.bert = bert
        with self.name_scope():
            self.classifier = nn.HybridSequential(prefix=prefix)
            if dropout:
                self.classifier.add(nn.Dropout(rate=dropout))
            self.classifier.add(nn.Dense(units=num_classes))

    def forward(self, inputs, token_types, valid_length=None):
        _, pooler = self.bert(inputs, token_types, valid_length)
        return self.classifier(pooler)

def load_model():
    global new_model
    global vocabulary
    global ctx
    global er
    
    er =np.loadtxt("data/256_label_info.txt" ,encoding='utf-8' ,delimiter='\t',skiprows=1,dtype=str)
    ctx = mx.gpu()
    bert_base, vocabulary = nlp.model.get_model('bert_12_768_12',
                                                 dataset_name='wiki_multilingual_cased',
                                                 pretrained=True, ctx=ctx, use_pooler=True,
                                                 use_decoder=False, use_classifier=False)  
    
    
    new_model = BERTClassifier(bert_base, num_classes=256, dropout=0.3) 

    new_model.load_parameters("add_comb_param/add_seg_comb25" ,ctx = ctx)
    print("load_model")
    
    
        
def main(test_text):
    # 순위 표시 위해 레이블링 된 텍스트 불러오기
    
    # 결과 값과 파일 명 매치
    
    
    def inference(model, data_iter, ctx=ctx):
        i = 0
        for i, (t,v,s) in enumerate(data_iter):
            token_ids = t.as_in_context(ctx)   #inputs
            valid_length = v.as_in_context(ctx) # valid_length
            segment_ids = s.as_in_context(ctx) # token_types
            output = model(token_ids, segment_ids, valid_length.astype('float32'))
            #model.summary(token_ids,segment_ids)
            if i > 1000:
                break
            i += 1
        y = nd.softmax(output[0])
        #y =    output[0]         
        return y  
    

    #########verify new_model##################################


    b = nlp.data.BERTTokenizer(vocabulary, lower=False)
    
    transform = nlp.data.BERTSentenceTransform(
            b, max_seq_length=250, pad=True, pair=False)
    sent_dataset = gluon.data.SimpleDataset([[test_text]])
       
    
    sentences = sent_dataset.transform(transform)        
    
    
    data_iter = mx.gluon.data.DataLoader(sentences, batch_size=1, num_workers=1)    


                 
    dic = {}    
    infer = inference(new_model,data_iter, ctx)
    infer = infer.asnumpy()
    result= []    
    for w in range(len(infer)) :
        result.append(infer[w]*100)
    

    for i in range(len(result)):
        dic[result[i]] = er[i][1]
        
    #print("####dic###")
    #print(dic)
    rank = sorted(dic.items() ,reverse = True)

    re_rank = []
    re_sim = []
    re_name =[]
    for ra in range(0,10):
        re_rank.append(str(ra+1)+"위 ")
        re_sim.append(str(round(rank[ra][0],2))+"%")
        re_name.append(rank[ra][1][:])
        #print(str(ra+1)+"위   : " , round(rank[ra][0],2),"%","| ", rank[ra][1])
        
    
    re_data = {"순위" : re_rank,"유사도" : re_sim , "계약서 명" : re_name}
    
    frame = pd.DataFrame(re_data)
    print(frame)  


if __name__=='__main__':
    load_model()
    txt = None
    while txt != "x":
        print("질의문을 입력하세요")
        txt = input(">>")
        print("질의문을 입력하세요")
        print("입력완료")
        print("추론 중입니다.....")
        main(txt) 
        print(" ")   
        print("추론 완료")
        